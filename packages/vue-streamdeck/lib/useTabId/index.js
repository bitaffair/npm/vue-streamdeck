'use strict';


const UUID = () => {
	return String(Math.floor(Math.random() * 1000000));
};


const STORE_KEY = 'tabId';



const TABID = window.sessionStorage[STORE_KEY] || UUID();


window.addEventListener("beforeunload", function(e) {
	window.sessionStorage[STORE_KEY] = TABID;
	return null;
});




export const useTabId = () => {
	return TABID;
};