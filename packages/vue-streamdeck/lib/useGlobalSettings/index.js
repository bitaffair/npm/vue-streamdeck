//




import { createGlobalState, useStorage } from '@vueuse/core';
import { reactive, computed, unref, toRef } from 'vue-demi';


const LS_MAPPING_KEY = 'streamdeck:global-settings';




export const useGlobalSettings = createGlobalState(() => {

	const DEFAULTS = {
		brightness: .5,
		wallpaperSrc: null,
		wallpaperText: ''
	};

	const store = useStorage(LS_MAPPING_KEY, DEFAULTS, localStorage, { mergeDefaults: true });

	return reactive({
		brightness: computed({
			get: () => unref(store).brightness,
			set: val => unref(store).brightness = parseFloat(val)
		}),
		wallpaperSrc: toRef(unref(store), 'wallpaperSrc'),
		wallpaperText: toRef(unref(store), 'wallpaperText')
	});

});