'use strict';

import { Buffer } from 'buffer';
import Color from 'color';


function wrapText(context, text, x, y, maxWidth, maxHeight, lineHeight) {
	let words = String(text).split(' ');
	let line = '';

	const lines = [];

	for (let [wordIndex, word] of Object.entries(words)) {
		let testLine = line + (line.length ? ' ' : '') + word;
		let { width } = context.measureText(testLine);

		if (width > maxWidth && wordIndex > 0) {
			lines.push(line);
			line = word;
		} else {
			line = testLine;
		}
	}

	lines.push(line);

	const totalHeight = lines.length * lineHeight;

	const topOffset = totalHeight < maxHeight ? ((maxHeight - totalHeight) / 2) : 0;

	for (let [index, line] of lines.entries()) {
		let i = index + 1;

		if ((i * lineHeight) > maxHeight) {
			break
		}

		context.fillText(line, x, topOffset + y + (i * lineHeight));
	}

}




export default ['basic', ({ ICON_SIZE }) => {


	const canvas = document.createElement('canvas')
	canvas.width = ICON_SIZE;
	canvas.height = ICON_SIZE;

	const ctx = canvas.getContext('2d', { willReadFrequently: true });


	const CANVAS_CENTER = canvas.width * .5;
	const CANVAS_PADDING = canvas.width * .02;
	const HEADER_HEIGHT = canvas.height * .28;
	const BUMPER_HEIGHT = canvas.height * .02;
	const FONT_SIZE = canvas.height * 0.19;
	const FONT_FAMILY = 'Helvetica';

	return {
		render: async (config, keyIndex) => {



			const FONT_COLOR = Color(config.color || 'white');
			const BG_COLOR = Color(config.backgroundColor || 'black');



			const indexLabel = String(('index' in config) ? config.index : (keyIndex + 1));


			//ctx.save()
			ctx.clearRect(0, 0, canvas.width, canvas.height);

			// bg
			ctx.fillStyle = BG_COLOR.rgb().string();
			ctx.fillRect(0, 0, canvas.width, canvas.height);

			// bg bottom
			ctx.fillStyle = FONT_COLOR.alpha(.2).rgb().string();
			ctx.fillRect(0, HEADER_HEIGHT, canvas.width, canvas.height - HEADER_HEIGHT);

			// bumper line
			ctx.fillStyle = FONT_COLOR.alpha(.5).rgb().string();
			ctx.fillRect(0, HEADER_HEIGHT, canvas.width, BUMPER_HEIGHT);


			// index label
			{
				const fontSize = FONT_SIZE * .9;
				ctx.font = `${fontSize}px "${FONT_FAMILY}"`
				ctx.textAlign = 'left';
				ctx.fillStyle = FONT_COLOR.alpha(.5).rgb().string();
				ctx.fillText(indexLabel, CANVAS_PADDING * 4, fontSize + CANVAS_PADDING)
			}

			// label
			{
				const fontSize = FONT_SIZE * .9;
				const lineHeight = 1.2;
				const top = HEADER_HEIGHT + BUMPER_HEIGHT
				ctx.font = `${fontSize}px "${FONT_FAMILY}"`
				ctx.textAlign = 'center';
				ctx.fillStyle = FONT_COLOR.rgb().string();
				wrapText(ctx, config.label, CANVAS_CENTER, top, canvas.width - (4 * CANVAS_PADDING), canvas.height - top - (4 * CANVAS_PADDING), fontSize * lineHeight);
			}


			const imgData = ctx.getImageData(0, 0, canvas.width, canvas.height)

			//ctx.restore()


			return {
				canvas,
				imgData,
				fillKeyBufferArgs: [Buffer.from(imgData.data), { format: 'rgba' }]
			}
		}
	}




}]