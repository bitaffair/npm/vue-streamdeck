'use strict';

import { reactive, set, isRef, del, computed, watch, ref, toRef, unref, effectScope, onScopeDispose, watchEffect, onBeforeUnmount } from 'vue-demi';
import { refDebounced, watchThrottled, useThrottleFn } from '@vueuse/core';

import { useDevices } from '../useDevices';


import { makePainter } from './painter';

import createObjectHash from 'object-hash';

import { DEVICE_USED } from '../symbols';



const useHidDevice = (serial, { layoutRef, keyStatesRef, onEvent, exclusive }) => {




	const SCOPE = effectScope(true);

	return SCOPE.run(() => {

		const devices = useDevices();

		let releaseLock;


		const _hidRef = computed(() => {

			const device = unref(devices.connected)?.[serial];

			if (!device) {
				return
			}

			return device;

		});

		const hidRef = ref(null);


		const initDevice = (device) => {

			if (device.isLocked) {
				return
			}

			if (exclusive) {
				releaseLock = device.lock();
			}

			if (device[DEVICE_USED] === true) {
				console.log('ups used twice');
			}
			device.clearPanel();
			device[DEVICE_USED] = true;

			hidRef.value = device;

		};


		if (unref(_hidRef)) {
			initDevice(unref(_hidRef));
		} else {


			const stopWatching = watch(() => unref(_hidRef), device => {

				if (!device) {
					return
				}

				stopWatching();

				initDevice(device);
			});

		}




		const painterRef = computed(() => {
			const device = unref(hidRef);

			if (!device) {
				return
			}

			return makePainter({ ICON_SIZE: device.ICON_SIZE });
		});


		const renderedViewsCache = new Map();

		const _tickKey = async (keyIndex) => {
			const hid = unref(hidRef);

			if (!hid) {
				return
			}

			const state = unref(keyStatesRef)?.[keyIndex] || 'default';
			const view = renderedViewsCache.get(`${keyIndex}.${state}`);

			if (!view) {
				return
			}

			await hid.fillKeyBuffer(keyIndex, ...view)

		};


		let isFirstPaint = false;

		const _paint = async () => {
			const layout = unref(layoutRef);
			const painter = unref(painterRef);
			const device = unref(hidRef);


			if (!layout || !painter) {
				return
			}

			if (isFirstPaint) {
				device.clearPanel();
			}

			try {

				const jobs = [];

				if ('keys' in layout) {

					for (let keyIndex = 0; keyIndex < device.NUM_KEYS; keyIndex++) {

						const keyConfig = layout.keys[keyIndex];

						if (!keyConfig) {
							continue;
						}


						jobs.push((async () => {


							const oldHash = _keyHashs.get(keyIndex);
							const newHash = createObjectHash(keyConfig);


							if (oldHash === newHash) {
								return
							}

							_keyHashs.set(keyIndex, newHash);

							if (keyConfig.views) {

								const defaultView = keyConfig.views.default;

								for (let [stateName, viewConfig] of Object.entries(keyConfig.views)) {

									const viewCacheId = `${keyIndex}.${stateName}`;
									const result = await painter.render(keyConfig.painter, Object.assign({}, defaultView, viewConfig), keyIndex);

									renderedViewsCache.set(viewCacheId, result.fillKeyBufferArgs);
								}
							}

							await _tickKey(keyIndex)
						})())

					}

				}

				await Promise.all(jobs);

			} catch (err) {
				console.log(err);
			}

		};

		const throtthledPaint = useThrottleFn(_paint);


		const _keyHashs = new Map();

		watch(() => unref(hidRef), (n, o) => {
			_keyHashs.clear();
			if (o) {
				//o.clearPanel();
			}
		}, { immediate: true })


		watch(layoutRef, throtthledPaint, { deep: true, immediate: true });
		watch(painterRef, throtthledPaint, { immediate: true });




		const eventHandler = e => {
			if (e.serial !== serial || !unref(hidRef)) {
				return;
			}

			onEvent(e);
		}


		devices.events.on('*', eventHandler);


		return {
			_tickKey,

			dispose() {

				const hid = unref(hidRef);
				SCOPE.stop();

				devices.events.off('*', eventHandler);



				if (hid) {
					hid[DEVICE_USED] = false;

					setTimeout(() => {
						if (hid[DEVICE_USED] === false) {

							hid.showWallpaper();

							if (exclusive && releaseLock) {
								releaseLock();
							}
						}
					}, 100);
				}


			}
		};

	});


};




// ----------------------------------------

const useMultiDevice = (serialsRef, options) => {
	const hidDevicesMap = new Map();

	const _layoutRef = options?.layout ? toRef(options, 'layout') : ref({ keys: {} });
	const layoutRef = refDebounced(_layoutRef, 100);



	const keyStatesRef = ref({});
	const latestEventRef = ref(null);



	const _tickKey = (keyIndex) => {
		for (let device of hidDevicesMap.values()) {
			device._tickKey(keyIndex);
		}
	};


	const setKeyState = (keyIndex, state) => {
		set(keyStatesRef.value, keyIndex, state);
		_tickKey(keyIndex);
	};


	const eventHandlerScope = {
		setKeyState
	};


	const eventHandler = (serial, e) => {
		const layout = unref(layoutRef);

		const handler = layout?.keys?.[e.key]?.events?.[e.type];

		latestEventRef.value = { key: e.key, type: e.type, serial };

		if (!handler) {
			return;
		}

		const currentState = unref(keyStatesRef)?.[e.key] || 'default';

		handler.apply(eventHandlerScope, [{ key: e.key, state: currentState }]);
	}




	watch(() => unref(serialsRef), (newSerials) => {

		if (!Array.isArray(newSerials)) {
			newSerials = [newSerials];
		}

		newSerials = newSerials.filter(Boolean);

		// remove outdated
		for (let outdatedSerial of Array.from(hidDevicesMap.keys()).filter(serial => !newSerials.includes(serial))) {
			const hidDevice = hidDevicesMap.get(outdatedSerial);
			hidDevice.dispose();
			hidDevicesMap.delete(outdatedSerial);
		}

		setTimeout(() => {

			// add new devices
			for (let serial of newSerials) {
				if (hidDevicesMap.has(serial)) {
					continue;
				}

				const hidDevice = useHidDevice(serial, {
					layoutRef,
					keyStatesRef,
					onEvent: e => eventHandler(serial, e),
					exclusive: options?.exclusive
				});

				hidDevicesMap.set(serial, hidDevice);
			}

		});


	}, { immediate: true });



	const disposeHids = () => {
		for (let hidDevice of hidDevicesMap.values()) {
			hidDevice.dispose();
		}
	};

	//window.addEventListener('beforeunload', disposeHids);

	onScopeDispose(() => {
		//	window.removeEventListener('beforeunload', disposeHids);
		disposeHids();
	});




	return reactive({
		layout: _layoutRef,
		latestEvent: latestEventRef,
		setKeyState,
		keyStates: keyStatesRef
	})

};



export const useDevice = useMultiDevice;