'use strict';


import PainterBasic from './key-painters/basic';


const PAINTER_FACTORIES = [
	PainterBasic
];


export const makePainter = factoryCradle => {

	const DEFAULT_KIND = PAINTER_FACTORIES[0][0];

	const devicePaintersMap = new Map(
		PAINTER_FACTORIES.map(([key, factory]) => ([key, factory(factoryCradle)]))
	);


	return {
		async render(kind, ...args) {

			const painter = devicePaintersMap.get(kind || DEFAULT_KIND);

			if (!painter) {
				return
			}

			try {
				return painter.render(...args);
			} catch (err) {
				console.log(err);
			}
		}

	}

}