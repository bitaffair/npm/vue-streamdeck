import { reactive, ref, computed, unref, effectScope, set, del } from 'vue-demi';
import { useStorage, createGlobalState } from '@vueuse/core';


const LS_MAPPING_KEY = 'streamDecks.mapper';



export const _useMapper = (name, options, parentMapper) => {

	const SCOPE = effectScope();

	const mapping = useStorage(
		LS_MAPPING_KEY + `:${name}`,
		(options.defaultMapping || {}),
		localStorage, { mergeDefaults: true }
	);




	const getSerial = (keyRef) => {
		const parentKey = unref(mapping)[unref(keyRef)];

		if (!parentMapper) {
			return parentKey
		}

		if (parentKey === null) {
			return null;
		}

		if (Array.isArray(parentKey)) {
			return parentKey.map(k => parentMapper.getSerial(k)).flat().filter((value, index, self) => {
				return self.indexOf(value) === index;
			});
		}

		return parentMapper.getSerial(parentKey);

	};


	return {
		_isMapper: true,
		mapping: computed(() => unref(mapping)),
		set: (key, val) => {
			key = String(key)
			const m = unref(mapping);


			for (let [k, v] of Object.entries(m)) {

				if (k === key) {
					continue;
				}

				if (Array.isArray(v)) {
					if (Array.isArray(val)) {
						m[k] = v.filter(i => !val.includes(i));
					} else {
						m[k] = v.filter(i => i !== val);
					}
				} else {
					if (Array.isArray(val)) {
						if (val.includes(v)) {
							m[k] = null;
						}
					} else {
						if (v === val) {
							m[k] = null;
						}
					}
				}

			}

			set(m, key, val);
		},


		del(key) {
			const m = unref(mapping);
			del(m, key);
		},

		getSerial,

		serial(keyRef) {
			return SCOPE.run(() => computed(() => getSerial(keyRef)));
		}
	}

}




export const useRootMapper = createGlobalState((options) => {
	return _useMapper('root', { allowDuplicates: false });
});




export const useMapper = (name, options) => {
	if (name === 'root') {
		return useRootMapper();
	}

	return _useMapper(name, {
		defaultMapping: options?.defaultMapping
	}, useRootMapper());
}