import { expect } from 'vitest';
import { unref, isRef } from 'vue'
import { useMapper, useRootMapper } from '.';




describe('mapping', () => {

	let rootMapper;
	beforeEach(() => {
		localStorage.clear();

		rootMapper = useRootMapper();

		rootMapper.set(0, 'A');
		rootMapper.set(1, 'B');
		rootMapper.set(2, 'C');
	});




	describe('useRootMapper', () => {

		it('should be defined', () => {
			expect(useRootMapper).toBeDefined();
		});

		describe('mapping', () => {
			it('should be a ref', () => {
				expect(isRef(rootMapper.mapping))
					.toEqual(true);
			});

			it('should contain an object with with pattern { key: serial }', () => {
				expect(unref(rootMapper.mapping))
					.toMatchObject({
						0: 'A',
						1: 'B',
						2: 'C'
					});
			});
		});

		describe('set', () => {

			it('should be defined', () => {
				expect(rootMapper.set)
					.toBeDefined()
			});

			it('should chnage mapping', () => {

				rootMapper.set(0, 'C');

				expect(unref(rootMapper.mapping))
					.toMatchObject({
						0: 'C',
						1: 'B',
						2: null
					});
			});

			it('should add new item in mapping', () => {

				rootMapper.set(3, 'D');

				expect(unref(rootMapper.mapping))
					.toMatchObject({
						0: 'A',
						1: 'B',
						2: 'C',
						3: 'D'
					});

			});


			it('should handle multiple', () => {

				rootMapper.set(3, 'D');

				rootMapper.set(0, ['A', 'B', 'C']);

				expect(unref(rootMapper.mapping))
					.toMatchObject({
						0: ['A', 'B', 'C'],
						1: null,
						2: null,
						3: 'D'
					});
			});



			it('should unify multiple', () => {

				rootMapper.set(3, 'D');
				rootMapper.set(0, ['A', 'B', 'C']);
				rootMapper.set(1, 'A');

				expect(unref(rootMapper.mapping))
					.toMatchObject({
						0: ['B', 'C'],
						1: 'A',
						2: null,
						3: 'D'
					});



			});

		});


		describe('del', () => {

			it('should remove item', () => {

				rootMapper.del(1);

				expect(unref(rootMapper.mapping))
					.toMatchObject({
						0: 'A',
						2: 'C'
					});

			});

		});

	});



	describe('useMapper', () => {


		it('should', () => {
			expect(useMapper).toBeDefined();
		});


		it('should have default mapping', () => {

			const { mapping } = useMapper('app-a', {
				defaultMapping: {
					'controls': 0,
					'chyrons': 1
				}
			});


			expect(mapping).toBeDefined();
			expect(unref(mapping)).toContain({
				controls: 0,
				chyrons: 1
			});

		});



		it('should change mapping', () => {

			const { mapping, set } = useMapper('app-a', {
				defaultMapping: {
					'controls': 0,
					'chyrons': 1
				}
			});

			set('controls', 1);

			expect(unref(mapping))
				.toMatchObject({
					controls: 1,
					chyrons: null
				});

		});


		describe('serial', () => {

			it.only('should return ref', () => {

				const appMapper = useMapper('app-a', {
					defaultMapping: {
						'controls': 0,
						'chyrons': 1
					}
				});

				const controlsSrial = appMapper.serial('controls');
				const chyronsSrial = appMapper.serial('chyrons');

				expect(unref(chyronsSrial))
					.to.equal('B');

				expect(unref(controlsSrial))
					.toEqual('A')

				rootMapper.set(0, 'C');

				expect(unref(chyronsSrial))
					.to.equal('B');

				expect(unref(controlsSrial))
					.toEqual('C');

				appMapper.set('controls', 1);

				expect(unref(chyronsSrial))
					.to.equal(null);

				expect(unref(controlsSrial))
					.toEqual('B');


			});


			it('should resolve multiple', () => {


				const appMapper = useMapper('app-a', {
					defaultMapping: {
						'controls': [0, 1],
					}
				});


				const controlsSrial = appMapper.serial('controls');

				expect(unref(controlsSrial))
					.toEqual(['A', 'B'])

			});


			it('should flatten result of multi', () => {


				rootMapper.set(0, ['A']);
				rootMapper.set(1, ['B']);


				const appMapper = useMapper('app-a', {
					defaultMapping: {
						'controls': [0, 1],
					}
				});


				const controlsSrial = appMapper.serial('controls');

				expect(unref(controlsSrial))
					.toEqual(['A', 'B'])


			});


			it('should unify multi result', () => {

				rootMapper.set(0, ['A', 'B']);
				rootMapper.set(1, ['B']);


				const appMapper = useMapper('app-a', {
					defaultMapping: {
						'controls': [0, 1, 0, 1],
					}
				});


				const controlsSrial = appMapper.serial('controls');

				expect(unref(controlsSrial))
					.toEqual(['A', 'B'])

			});


		})


	});




});