//




import { unref, del, set, toRaw } from 'vue-demi';
import { useStorage, createGlobalState } from '@vueuse/core';
import createDebug from 'debug';


import { useTabId } from '../useTabId';


export const useDeviceLockStore = createGlobalState(() => {

	const TAB_ID = useTabId();

	const log = createDebug(`DeviceLockStore:${TAB_ID}`);

	// shared store in localstorage of locked devices
	const lockedDevices = useStorage('streamdeck:locked-devices', {}, localStorage, { mergeDefaults: true });


	// local store in memory of current tab
	//let lockedDevicesLocal = null;

	// release all devices of current tab in shared lock store
	// window.addEventListener('beforeunload', () => {
	// 	if (lockedDevicesLocal && lockedDevicesLocal.length) {
	// 		lockedDevices.value = lockedDevices.value.filter(serial => !lockedDevicesLocal.includes(serial));
	// 	}
	// });


	log('Creating BroadcastChannel for Tab');
	const channel = new BroadcastChannel(`tab:${TAB_ID}`);
	channel.addEventListener('message', e => {
		if (e.data === 'ping') {
			log('Received pong from BroadcastChannel');
			channel.postMessage('pong');
		}
	})



	const isDeviceInUse = serial => {
		const lock = unref(lockedDevices)[serial];

		if (!lock) {
			return false;
		}

		if (lock.tab === TAB_ID) {
			return false;
		}

		cleanupLocking(serial, lock);

		return true;
	};



	const lockDevice = serial => {
		log('Locking device', { serial });
		set(lockedDevices.value, serial, { tab: TAB_ID });
	};

	const releaseDevice = serial => {
		log('Releasing device', { serial });
		del(lockedDevices.value, serial);
	};



	const cleanupLocking = async (serial, lock) => {

		if (!lock || (lock.tab === TAB_ID)) {
			return true;
		}

		const remoteChannel = new BroadcastChannel(`tab:${lock.tab}`);

		const isAlive = await new Promise((resolve, reject) => {
			let timeout;

			remoteChannel.addEventListener('message', e => {
				if (e.data === 'pong') {
					clearTimeout(timeout);
					resolve(true);
				}
			});

			log(`Requesting remote tab(${lock.tab})`, { serial });
			remoteChannel.postMessage('ping');
			setTimeout(() => resolve(false), 100);
		});

		remoteChannel.close();

		log(`Remote tab(${lock.tab}) is alive`, isAlive);

		if (!isAlive) {
			releaseDevice(serial);
		}

	};




	return {
		isDeviceInUse,
		lockDevice,
		releaseDevice
	}

});