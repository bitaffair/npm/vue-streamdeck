import { watch, unref } from 'vue';
import { Buffer } from 'buffer';
import { cover } from 'intrinsic-scale';

const getImg = async (src) => {

	if (!src) {
		return
	}

	const wpImg = new Image();
	wpImg.setAttribute('crossOrigin', '')
	await new Promise((resolve, reject) => {
		wpImg.onload = resolve;
		wpImg.onerror = reject;
		wpImg.src = src;
	});

	return wpImg;
};




async function renderWallpaper({ device, globalSettings }) {




	const canvas = document.createElement('canvas')
	canvas.width = device.ICON_SIZE * device.KEY_COLUMNS;
	canvas.height = device.ICON_SIZE * device.KEY_ROWS;


	const ctx = canvas.getContext('2d', { willReadFrequently: true });

	ctx.save()
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = '#222';

	ctx.fillRect(0, 0, canvas.width, canvas.height);


	if (globalSettings.wallpaperSrc) {

		const img = await getImg(globalSettings.wallpaperSrc);

		const { width, height, x, y } = cover(canvas.width, canvas.height, img.width, img.height);
		ctx.drawImage(img, x, y, width, height);

	} else {

		const FONT_FAMILY = 'Arial';

		const text = globalSettings.wallpaperText || 'BA';

		ctx.fillStyle = `rgba(255,255,255, .5)`;

		ctx.font = `bold ${canvas.height}px "${FONT_FAMILY}"`
		ctx.textAlign = 'center';

		const { width: measuredWith } = ctx.measureText(text);


		const fontSize = Math.min(canvas.height * 1.2, canvas.height * ((canvas.width * .9) / measuredWith));
		ctx.font = `bold ${fontSize}px "${FONT_FAMILY}"`

		ctx.fillText(text, (canvas.width / 2), (canvas.height / 2) + (fontSize / 2 * .72))
	}

	const imgData = ctx.getImageData(0, 0, canvas.width, canvas.height)

	const buffer = [Buffer.from(imgData.data), { format: 'rgba' }];
	ctx.restore()

	return {
		buffer
	};

}



export const useWallpaperCtrl = (device, { globalSettings }) => {

	device.wallpaper = null;


	device._initJobs.push(async () => device.wallpaper = await renderWallpaper({ device, globalSettings }));


	// update wallpapers on all devices
	watch(() => [globalSettings.wallpaperSrc, globalSettings.wallpaperText], async () => {
		device.wallpaper = await renderWallpaper({ device, globalSettings });

		if (device._isUsed !== true) {
			device.showWallpaper();
		}
	});

};