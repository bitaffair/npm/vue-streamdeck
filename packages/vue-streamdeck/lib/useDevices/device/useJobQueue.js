//




class Queue {

	constructor(hid) {
		this.hid = hid;
		this._jobs = [];
		this._currentPromise = null;
	}

	_tick() {
		if (this._currentPromise) {
			return
		}

		const job = this._jobs.shift();
		if (!job) {
			return
		}

		this._currentPromise = this.hid[job[0]].apply(this.hid, job[1])
			.then(() => {
				this._currentPromise = null;
				this._tick();
			})
			.catch(err => {
				console.log(err);
				this._currentPromise = null;
				this._tick();
			});

	}

	add(...job) {
		this._jobs.push(job);
		this._tick();
	}


}



export const useJobQueue = device => {
	return new Queue(device.hid);
}