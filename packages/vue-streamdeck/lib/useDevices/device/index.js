//

import { useWallpaperCtrl } from './useWallpaperCtrl';
import { useJobQueue } from './useJobQueue';



export default class Device {

	constructor(serial, hid, cradle) {

		this.serial = serial;
		this._deviceLockStore = cradle.deviceLockStore;
		this.hid = hid;


		this._initJobs = [];


		this._queue = useJobQueue(this);
		this._wallpaperCtrl = useWallpaperCtrl(this, cradle);
	}

	async init() {
		for (let job of this._initJobs) {
			await job();
		}
	}

	get isLocked() {
		return this._deviceLockStore.isDeviceInUse(this.serial);
	}

	lock() {
		this._deviceLockStore.lockDevice(this.serial);
		return () => {
			this._deviceLockStore.releaseDevice(this.serial);
		}
	}

	fillKeyBuffer(...args) {
		this._queue.add('fillKeyBuffer', args);
	}

	fillPanelBuffer(...args) {
		this._queue.add('fillPanelBuffer', args);
	}

	setBrightness(...args) {
		this._queue.add('setBrightness', args);
	}

	clearPanel(...args) {
		this._queue.add('clearPanel', args);
	}

	showWallpaper() {
		if (this.wallpaper?.buffer) {
			this.fillPanelBuffer(...this.wallpaper?.buffer);
		} else {
			this.clearPanel();
		}
	}

	get PRODUCT_NAME() {
		return this.hid.PRODUCT_NAME;
	}

	get ICON_SIZE() {
		return this.hid.ICON_SIZE;
	}

	get KEY_COLUMNS() {
		return this.hid.KEY_COLUMNS;
	}

	get KEY_ROWS() {
		return this.hid.KEY_ROWS;
	}

	get NUM_KEYS() {
		return this.hid.NUM_KEYS;
	}

}