import { getStreamDecks, requestStreamDecks } from '@elgato-stream-deck/webhid'
import { reactive, toRef, set, unref, watch, effectScope } from 'vue-demi';
import { EventEmitter } from 'events';
import { Buffer } from 'buffer';


import { createGlobalState } from '@vueuse/core';

import { DEVICE_USED } from '../symbols';

import { useGlobalSettings } from '../useGlobalSettings';
import { useDeviceLockStore } from '../useDeviceLockStore';


import QueueDevice from './device';



export const useDevices = createGlobalState(() => {

	const SCOPE = effectScope(true);

	const globalSettings = useGlobalSettings();
	const deviceLockStore = useDeviceLockStore();


	const state = reactive({
		devices: {}
	});



	const events = new EventEmitter();




	const _upsertDevices = (devices) => SCOPE.run(async () => {
		for (let _device of devices) {
			const serial = await _device.getSerialNumber();

			const device = new QueueDevice(serial, _device, { deviceLockStore, globalSettings });

			await device.init();



			if (!device.isLocked) {
				const brightness = unref(globalSettings.brightness);
				await device.setBrightness(brightness * 100);
				await device.showWallpaper();
			}

			set(state.devices, serial, device);

			_device.on('down', (key) => {
				const event = { serial, key, type: 'down' };
				events.emit('*', event)
			});

			_device.on('up', (key) => {
				const event = { serial, key, type: 'up' };
				events.emit('*', event)
			});

		}
	});


	const requestDevices = async () => {
		try {
			const devices = await requestStreamDecks();
			await _upsertDevices(devices);
		} catch (err) {
			console.log(err);
		}
	};

	const initDevices = async () => {
		try {
			const devices = await getStreamDecks();
			await _upsertDevices(devices);
		} catch (err) {
			console.log(err)
		}
	};

	initDevices();


	// update brightness on all devices
	watch(() => unref(globalSettings.brightness), brightness => {
		for (let device of Object.values(state.devices)) {
			device.setBrightness(brightness * 100);
		}
	});




	return ({
		connected: toRef(state, 'devices'),
		requestDevices,
		events
	})




});