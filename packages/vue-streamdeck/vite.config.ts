import { defineConfig } from 'vite';
import Vue2 from '@vitejs/plugin-vue2';
import nodePolyfills from 'vite-plugin-node-stdlib-browser';



export default defineConfig({
  plugins: [
    nodePolyfills(),
    Vue2(),
  ],
  test: {
    globals: true,
    environment: 'jsdom',
    alias: [{ find: /^vue$/, replacement: 'vue/dist/vue.runtime.common.js' }],
  }

})