
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

import { createRouter, createWebHashHistory } from 'vue-router';


const routes = [
	{ path: '/', component: () => import('./tabs/home.vue') },
	{ path: '/case-1', component: () => import('./tabs/case-1.vue') },
	{ path: '/painters', component: () => import('./tabs/painters.vue') },
]

const router = createRouter({
	// 4. Provide the history implementation to use. We are using the hash history for simplicity here.
	history: createWebHashHistory(),
	routes, // short for `routes: routes`
})




const app = createApp(App)

app.use(router);

app.mount('#app')